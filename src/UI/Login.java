package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import java.awt.Color;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;

import wala.Articulos;
import wala.Conexion;
import wala.User;
import javax.swing.UIManager;
public class Login {

	private JFrame frame;
	private JTextField mail;
	private JTextField pass;
	private JLabel lblContraseaOUsuario;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.weightx = 1.0;
		gbc_rigidArea.weighty = 1.0;
		gbc_rigidArea.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea.gridx = 0;
		gbc_rigidArea.gridy = 3;
		frame.getContentPane().add(rigidArea, gbc_rigidArea);
		
		lblContraseaOUsuario = new JLabel("Login incorrecto");
		GridBagConstraints gbc_lblContraseaOUsuario = new GridBagConstraints();
		gbc_lblContraseaOUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_lblContraseaOUsuario.gridx = 2;
		gbc_lblContraseaOUsuario.gridy = 5;
		frame.getContentPane().add(lblContraseaOUsuario, gbc_lblContraseaOUsuario);
		lblContraseaOUsuario.setVisible(false);
		lblContraseaOUsuario.setForeground(Color.RED);
		
		JLabel lblNewLabel = new JLabel("Mail");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 6;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		mail = new JTextField();
		mail.setText("guillermolmr@gmail.com");
		GridBagConstraints gbc_mail = new GridBagConstraints();
		gbc_mail.insets = new Insets(0, 0, 5, 5);
		gbc_mail.anchor = GridBagConstraints.WEST;
		gbc_mail.gridx = 2;
		gbc_mail.gridy = 6;
		frame.getContentPane().add(mail, gbc_mail);
		mail.setColumns(10);
		
		JLabel lblPass = new JLabel("Pass");
		GridBagConstraints gbc_lblPass = new GridBagConstraints();
		gbc_lblPass.insets = new Insets(0, 0, 5, 5);
		gbc_lblPass.anchor = GridBagConstraints.EAST;
		gbc_lblPass.gridx = 1;
		gbc_lblPass.gridy = 7;
		frame.getContentPane().add(lblPass, gbc_lblPass);
		
		pass = new JPasswordField();
		pass.setFont(UIManager.getFont("PasswordField.font"));
		pass.setText("1234");
		GridBagConstraints gbc_pass = new GridBagConstraints();
		gbc_pass.insets = new Insets(0, 0, 5, 5);
		gbc_pass.anchor = GridBagConstraints.WEST;
		gbc_pass.gridx = 2;
		gbc_pass.gridy = 7;
		frame.getContentPane().add(pass, gbc_pass);
		pass.setColumns(10);
		
		JButton btnNewButton = new JButton("Login");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 3;
		gbc_btnNewButton.gridy = 7;
		frame.getContentPane().add(btnNewButton, gbc_btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(User.logUsu(new Conexion(), mail.getText(), pass.getText())){
					frame.setVisible(false);
					Grid g=new Grid(new Articulos(User.getConexion()));
					g.frame.setVisible(true);
					
				}else{
					lblContraseaOUsuario.setVisible(true);
				}
			
				
			}
		});
	}

}
