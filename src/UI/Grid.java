package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;

import wala.Articulos;
import wala.Categoria;
import wala.User;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JSlider;
import javax.swing.JComboBox;

public class Grid {

	public JFrame frame;
	private JPanel panel;
	public static Articulos articulos;
	public static Categoria categoria;
	private JPanel pagination;
	private int pagina;
	public static int num_paginas;
	private int subcatego;
	
	private int apa�o;
	
	private JTextField searchName;
	private JPanel search;
	private JLabel showprecio;
	private JCheckBox searchEnvio;
	private JCheckBox searchInter;
	private JCheckBox chckbxNegociable;
	private JComboBox Provincia;
	private JComboBox Localidad;
	private JSlider slider;
	
	public static Grid grid;
	
	/**
	 * options:
	 * 	0: my items
	 * 	1: search
	 * 	2: catego
	 * 	3: subcatego
	 */
	public int option=0;
	
	
	
	/**
	 * Create the application.
	 */
	public Grid(Articulos articulos) {
		grid=this;
		this.articulos=articulos;
		this.categoria=new Categoria(User.getConexion());
		
		pagina=1;
		num_paginas=articulos.contarMisArticulos()/5+1;
		
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1400, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 72, 48, 0, 0, 0, 58, 51, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{};
		gbl_panel.rowWeights = new double[]{};
		panel.setLayout(gbl_panel);
		pagination = new JPanel();
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnOpciones = new JMenu("Opciones");
		menuBar.add(mnOpciones);
		
		JMenuItem mntmAadir = new JMenuItem("A\u00F1adir");
		mntmAadir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				VistaArticulo vista= new VistaArticulo(Grid.articulos, true, true, 0, "", "", 0f,false,false,false, User.getId(), "",1);
				vista.frame.setVisible(true);
				
			}
		});
		mnOpciones.add(mntmAadir);
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mntmBuscar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				search.setVisible(true);
				
			}
		});
		mnOpciones.add(mntmBuscar);
		
		JMenuItem mntmMisArticulos = new JMenuItem("Mis articulos");
		
		mntmMisArticulos.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				search.setVisible(false);
				option=0;
				pagina=1;
				updateList();
				
			}
		});
		mnOpciones.add(mntmMisArticulos);
		
		JMenu mnCategorias = new JMenu("Categorias");
		menuBar.add(mnCategorias);
		Categoria.Catego categos[]=categoria.getCategorias();
		for(int i=0;i<categos.length;i++){
			JMenu mnCategoria = new JMenu(categos[i].nombre);
			mnCategorias.add(mnCategoria);
			Categoria.SubCatego[] subcategos=categoria.getSubCategorias(categos[i].id);
			System.out.println(subcategos.length);
			for(int e=0;e<subcategos.length;e++){
				apa�o=e;
				
				JMenuItem mntmCategoria = new JMenuItem(subcategos[e].nombre);
				mnCategoria.add(mntmCategoria);
				mntmCategoria.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent ae) {
						option=2;
						subcatego=subcategos[apa�o].id;
						
					}
				});
			}
		}
		
		
		
		updateList();
		
		search = new JPanel();
		FlowLayout flowLayout = (FlowLayout) search.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		frame.getContentPane().add(search, BorderLayout.NORTH);
		
		JLabel lblNombre = new JLabel("Nombre");
		search.add(lblNombre);
		
		searchName = new JTextField();
		search.add(searchName);
		searchName.setColumns(10);
		
		Component rigidArea_1 = Box.createRigidArea(new Dimension(20, 20));
		search.add(rigidArea_1);
		
		JLabel lblPrecio = new JLabel("Precio");
		search.add(lblPrecio);
		
		slider = new JSlider();
		slider.setPaintTicks(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				int value=((JSlider)e.getSource()).getValue();
				if(value==0){
					showprecio.setText("cualquiera");
				}else{
					showprecio.setText("<"+value);
				}
				
			}
		});
		search.add(slider);
		
		showprecio = new JLabel("cualquiera");
		search.add(showprecio);
		
		Component rigidArea_2 = Box.createRigidArea(new Dimension(20, 20));
		search.add(rigidArea_2);
		
		searchEnvio = new JCheckBox("Envio");
		search.add(searchEnvio);
		
		searchInter = new JCheckBox("Intercambiable");
		search.add(searchInter);
		
		chckbxNegociable = new JCheckBox("Negociable");
		search.add(chckbxNegociable);
		
		JLabel lblProvincia = new JLabel("Provincia");
		search.add(lblProvincia);
		
		Provincia = new JComboBox(wala.Localidad.getProvincias());
		Provincia.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Localidad.removeAllItems();
				String prov=(String) ((JComboBox)e.getSource()).getSelectedItem();
				String[] s=wala.Localidad.getLocalidades(prov);
				for(int i=0;i<s.length;i++){
					Localidad.addItem(s[i]);
				}
			}
		});
		
		search.add(Provincia);
		
		JLabel lblLocalidad_1 = new JLabel("Localidad");
		search.add(lblLocalidad_1);
		
		Localidad = new JComboBox(new String[]{""});
		search.add(Localidad);
		
		JLabel lblsoloBuscaPor = new JLabel("*Solo busca por localidad");
		search.add(lblsoloBuscaPor);
		
		JButton btnSearch = new JButton("Search");
		search.add(btnSearch);
		btnSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				option=1;
				pagina=1;
				updateList();
				
			}
		});
		search.setVisible(false);
		
		
		
	}
	private void updatePag(){
		System.out.println("");
		pagination.removeAll();
		if(pagina>1){
			addPag("<<");
			addPag("<");
		}
		for(int i=0;i<num_paginas;){
			i++;
			addPag(i+"");
		}
		if(pagina<num_paginas){
			addPag(">");
			addPag(">>");
		}
		
	}
	private void addPag(String i){
		JButton btn1 = new JButton(i);
		if(i.equals(""+pagina)){
			btn1.setEnabled(false);
		}
		btn1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			
				String s=((JButton)e.getSource()).getText();
				System.out.println(pagina);
				switch(s){
					case "<<": 
						pagina=1;
						break;
					case "<":
						pagina--;
						break;
					case ">":
						pagina++;
					case ">>":
						pagina=num_paginas;
						break;
					default:
						pagina=Integer.parseInt(s);
						break;
				}
				updateList();
				
				
			}
		});
		pagination.add(btn1);
	}
	public void updateList(){
		
		panel.removeAll();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 72, 48, 0, 0, 0, 58, 51, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		
		
		JLabel foto = new JLabel("Foto");
		GridBagConstraints gbc_foto = new GridBagConstraints();
		gbc_foto.insets = new Insets(0, 0, 5, 5);
		gbc_foto.gridx = 0;
		gbc_foto.gridy = 0;
		panel.add(foto, gbc_foto);
		
		JLabel nombre = new JLabel("Nombre");
		GridBagConstraints gbc_nombre = new GridBagConstraints();
		gbc_nombre.anchor = GridBagConstraints.WEST;
		gbc_nombre.insets = new Insets(0, 0, 5, 5);
		gbc_nombre.gridx = 1;
		gbc_nombre.gridy = 0;
		panel.add(nombre, gbc_nombre);
		
		JLabel precio = new JLabel("precio");
		GridBagConstraints gbc_precio = new GridBagConstraints();
		gbc_precio.anchor = GridBagConstraints.WEST;
		gbc_precio.insets = new Insets(0, 0, 5, 5);
		gbc_precio.gridx = 2;
		gbc_precio.gridy = 0;
		panel.add(precio, gbc_precio);
		
		JLabel envio = new JLabel("envio");
		GridBagConstraints gbc_envio = new GridBagConstraints();
		gbc_envio.anchor = GridBagConstraints.WEST;
		gbc_envio.insets = new Insets(0, 0, 5, 5);
		gbc_envio.gridx = 3;
		gbc_envio.gridy = 0;
		panel.add(envio, gbc_envio);
		
		JLabel lblIntercambio = new JLabel("intercambio");
		GridBagConstraints gbc_lblIntercambio = new GridBagConstraints();
		gbc_lblIntercambio.anchor = GridBagConstraints.WEST;
		gbc_lblIntercambio.insets = new Insets(0, 0, 5, 5);
		gbc_lblIntercambio.gridx = 4;
		gbc_lblIntercambio.gridy = 0;
		panel.add(lblIntercambio, gbc_lblIntercambio);
		
		JLabel lblNegociable = new JLabel("negociable");
		GridBagConstraints gbc_lblNegociable = new GridBagConstraints();
		gbc_lblNegociable.anchor = GridBagConstraints.WEST;
		gbc_lblNegociable.insets = new Insets(0, 0, 5, 5);
		gbc_lblNegociable.gridx = 5;
		gbc_lblNegociable.gridy = 0;
		panel.add(lblNegociable, gbc_lblNegociable);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
		gbc_lblLocalidad.anchor = GridBagConstraints.WEST;
		gbc_lblLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocalidad.gridx = 6;
		gbc_lblLocalidad.gridy = 0;
		panel.add(lblLocalidad, gbc_lblLocalidad);
		
		JLabel lblUsuario = new JLabel("Usuario");
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.anchor = GridBagConstraints.WEST;
		gbc_lblUsuario.insets = new Insets(0, 0, 5, 5);
		gbc_lblUsuario.gridx = 7;
		gbc_lblUsuario.gridy = 0;
		panel.add(lblUsuario, gbc_lblUsuario);
		
		exeOption();
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.weighty = 1.0;
		gbc_rigidArea.anchor = GridBagConstraints.NORTH;
		gbc_rigidArea.insets = new Insets(0, 0, 0, 5);
		gbc_rigidArea.gridx = 0;
		gbc_rigidArea.gridy = 17;
		panel.add(rigidArea, gbc_rigidArea);
		
		frame.getContentPane().add(pagination, BorderLayout.SOUTH);
		pagination.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		
		updatePag();
		frame.repaint();
		frame.validate();
	}
	private void exeOption(){
		switch(option){
			case 0:
				//mostrar mis articulos
				articulos.getMisArticulos(panel, pagina);
				break;
			case 1:
				//mostrar articulos
				articulos.buscarArticulos(panel, pagina, searchName.getText(), slider.getValue(), searchEnvio.isSelected(), searchInter.isSelected(), chckbxNegociable.isSelected(), (String) Localidad.getSelectedItem());
				break;
			case 2:
					
				break;
			case 3:
				//mostrar subcategorias
				break;
				
			
		}
	}
	
}
