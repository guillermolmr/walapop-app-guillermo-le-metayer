package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import wala.Articulos;
import wala.Categoria;
import wala.Localidad;
import wala.Categoria.SubCatego;

import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class VistaArticulo {

	public JFrame frame;
	private JTextField textNombre;
	private boolean propietario;
	private boolean nuevo;
	private int id;
	private String nombre;
	private String descripcion;
	private boolean envio;
	private boolean intercambio;
	private boolean negociable;
	private String vendedor;
	private String provincia;
	private String localidad;
	private Articulos articulo;
	private JComboBox boxLocalidad;
	private JComboBox boxProvincia;
	private JTextField textPrecio;
	private float precio;
	
	private JComboBox<Categoria.Catego> boxCatego;
	private JComboBox<Categoria.SubCatego> boxSubCatego;
	
	//private String subcatego;
	

	private Categoria.Catego catego;
	private Categoria.SubCatego subcatego;
	



	
	/**
	 * Create the application.
	 */
	public VistaArticulo(Articulos articulo,boolean propietario,boolean nuevo,int id,String nombre,String descripcion,float precio,boolean envio,boolean intercambio,boolean negociable,String vendedor,String localidad,int subcategoria) {
		this.propietario=propietario;
		this.nuevo=nuevo;
		this.id=id;
		this.nombre=nombre;
		this.descripcion=descripcion;
		this.vendedor=vendedor;
		this.provincia=Localidad.findProvincia(localidad);
		this.localidad=localidad;
		this.articulo=articulo;
		this.precio=precio;
		this.envio=envio;
		this.intercambio=intercambio;
		this.negociable=negociable;
		catego=Grid.categoria.findCatego(subcategoria);
		subcatego=Grid.categoria.getSubcatego(subcategoria);
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.insets = new Insets(0, 0, 5, 5);
		gbc_rigidArea.gridx = 0;
		gbc_rigidArea.gridy = 0;
		frame.getContentPane().add(rigidArea, gbc_rigidArea);
		
		JLabel foto = new JLabel("foto");
		GridBagConstraints gbc_foto = new GridBagConstraints();
		gbc_foto.insets = new Insets(0, 0, 5, 5);
		gbc_foto.gridx = 1;
		gbc_foto.gridy = 1;
		frame.getContentPane().add(foto, gbc_foto);
		
		JLabel lblNombre = new JLabel("nombre");
		
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.anchor = GridBagConstraints.EAST;
		gbc_lblNombre.gridx = 3;
		gbc_lblNombre.gridy = 2;
		frame.getContentPane().add(lblNombre, gbc_lblNombre);
		
		textNombre = new JTextField(nombre);
		textNombre.setEditable(propietario);
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.gridx = 4;
		gbc_textField.gridy = 2;
		frame.getContentPane().add(textNombre, gbc_textField);
		textNombre.setColumns(10);
		
		JLabel lblVendedor = new JLabel("Vendedor");
		GridBagConstraints gbc_lblVendedor = new GridBagConstraints();
		gbc_lblVendedor.anchor = GridBagConstraints.EAST;
		gbc_lblVendedor.insets = new Insets(0, 0, 5, 5);
		gbc_lblVendedor.gridx = 3;
		gbc_lblVendedor.gridy = 3;
		frame.getContentPane().add(lblVendedor, gbc_lblVendedor);
		
		JLabel lblUsu = new JLabel(vendedor);
		System.out.println(vendedor);
		GridBagConstraints gbc_lblUsu = new GridBagConstraints();
		gbc_lblUsu.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblUsu.insets = new Insets(0, 0, 5, 0);
		gbc_lblUsu.gridx = 4;
		gbc_lblUsu.gridy = 3;
		frame.getContentPane().add(lblUsu, gbc_lblUsu);
		
		JLabel lblProvincia = new JLabel("Provincia");
		GridBagConstraints gbc_lblProvincia = new GridBagConstraints();
		gbc_lblProvincia.anchor = GridBagConstraints.EAST;
		gbc_lblProvincia.insets = new Insets(0, 0, 5, 5);
		gbc_lblProvincia.gridx = 3;
		gbc_lblProvincia.gridy = 4;
		frame.getContentPane().add(lblProvincia, gbc_lblProvincia);
		
		
		GridBagConstraints gbc_showprovincia = new GridBagConstraints();
		gbc_showprovincia.anchor = GridBagConstraints.WEST;
		gbc_showprovincia.insets = new Insets(0, 0, 5, 0);
		gbc_showprovincia.gridx = 4;
		gbc_showprovincia.gridy = 4;
		
		GridBagConstraints gbc_showlocalidad = new GridBagConstraints();
		gbc_showlocalidad.anchor = GridBagConstraints.WEST;
		gbc_showlocalidad.insets = new Insets(0, 0, 5, 0);
		gbc_showlocalidad.gridx = 4;
		gbc_showlocalidad.gridy = 5;
		
		
		GridBagConstraints gbc_lblCatego = new GridBagConstraints();
		gbc_lblCatego.anchor= GridBagConstraints.WEST;
		gbc_lblCatego.insets = new Insets(0, 0, 5, 0);
		gbc_lblCatego.gridx = 4;
		gbc_lblCatego.gridy = 8;
		
		GridBagConstraints gbc_lblSubcatego = new GridBagConstraints();
		gbc_lblSubcatego.anchor= GridBagConstraints.WEST;
		gbc_lblSubcatego.insets = new Insets(0, 0, 5, 0);
		gbc_lblSubcatego.gridx = 4;
		gbc_lblSubcatego.gridy = 9;
		
		if(propietario){
			
			boxCatego=new JComboBox<Categoria.Catego>(Grid.categoria.getCategorias());
			boxCatego.setSelectedItem(catego);
			frame.getContentPane().add(boxCatego, gbc_lblCatego);
			boxSubCatego=new JComboBox<Categoria.SubCatego>(Grid.categoria.getSubCategorias(catego.id));
			boxSubCatego.setSelectedItem(subcatego);
			frame.getContentPane().add(boxSubCatego, gbc_lblSubcatego);
			
			boxProvincia=new JComboBox(Localidad.getProvincias());
			boxProvincia.setSelectedItem(provincia);
			boxProvincia.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					boxLocalidad.removeAllItems();
					String prov=(String) ((JComboBox)e.getSource()).getSelectedItem();
					String[] s=Localidad.getLocalidades(prov);
					for(int i=0;i<s.length;i++){
						boxLocalidad.addItem(s[i]);
					}
					
				}
			});
			frame.getContentPane().add(boxProvincia, gbc_showprovincia);
			
			boxLocalidad=new JComboBox(Localidad.getLocalidades(provincia));
			boxLocalidad.setSelectedItem(localidad);
			frame.getContentPane().add(boxLocalidad, gbc_showlocalidad);
			
			
			
			
			
		}else{
			
			JLabel lblCatego = new JLabel("catego");
			frame.getContentPane().add(lblCatego, gbc_lblCatego);
			JLabel lblSubcatego = new JLabel(subcatego.toString());
			frame.getContentPane().add(lblSubcatego, gbc_lblSubcatego);
			
			
			JLabel showprovincia = new JLabel("provincia");
			
			frame.getContentPane().add(showprovincia, gbc_showprovincia);
			
			JLabel showlocalidad = new JLabel("localidad");
			
			frame.getContentPane().add(showlocalidad, gbc_showlocalidad);
		}
		JLabel lblLocalidad = new JLabel("Localidad");
		GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
		gbc_lblLocalidad.anchor = GridBagConstraints.EAST;
		gbc_lblLocalidad.insets = new Insets(0, 0, 5, 5);
		gbc_lblLocalidad.gridx = 3;
		gbc_lblLocalidad.gridy = 5;
		frame.getContentPane().add(lblLocalidad, gbc_lblLocalidad);
		
		JLabel lblPrecio = new JLabel("Precio");
		GridBagConstraints gbc_lblPrecio = new GridBagConstraints();
		gbc_lblPrecio.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrecio.gridx = 3;
		gbc_lblPrecio.gridy = 6;
		frame.getContentPane().add(lblPrecio, gbc_lblPrecio);
		
		textPrecio = new JTextField(precio+"�");
		textPrecio.setEditable(propietario);
		GridBagConstraints gbc_textPrecio = new GridBagConstraints();
		gbc_textPrecio.fill = GridBagConstraints.HORIZONTAL;
		gbc_textPrecio.insets = new Insets(0, 0, 5, 0);
		gbc_textPrecio.gridx = 4;
		gbc_textPrecio.gridy = 6;
		frame.getContentPane().add(textPrecio, gbc_textPrecio);
		
		JCheckBox chckbxEnvio = new JCheckBox("Envio");
		chckbxEnvio.setEnabled(propietario);
		chckbxEnvio.setSelected(envio);
		GridBagConstraints gbc_chckbxEnvio = new GridBagConstraints();
		gbc_chckbxEnvio.anchor = GridBagConstraints.EAST;
		gbc_chckbxEnvio.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxEnvio.gridx = 2;
		gbc_chckbxEnvio.gridy = 7;
		frame.getContentPane().add(chckbxEnvio, gbc_chckbxEnvio);
		
		JCheckBox chckbxIntercambio = new JCheckBox("Intercambio");
		chckbxIntercambio.setSelected(intercambio);
		chckbxIntercambio.setEnabled(propietario);
		GridBagConstraints gbc_chckbxIntercambio = new GridBagConstraints();
		gbc_chckbxIntercambio.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxIntercambio.gridx = 3;
		gbc_chckbxIntercambio.gridy = 7;
		frame.getContentPane().add(chckbxIntercambio, gbc_chckbxIntercambio);
		
		JCheckBox chckbxNegociable = new JCheckBox("Negociable");
		chckbxNegociable.setSelected(negociable);
		chckbxNegociable.setEnabled(propietario);
		GridBagConstraints gbc_chckbxNegociable = new GridBagConstraints();
		gbc_chckbxNegociable.anchor = GridBagConstraints.WEST;
		gbc_chckbxNegociable.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxNegociable.gridx = 4;
		gbc_chckbxNegociable.gridy = 7;
		frame.getContentPane().add(chckbxNegociable, gbc_chckbxNegociable);
		
		JLabel lblCategoria = new JLabel("Categoria");
		GridBagConstraints gbc_lblCategoria = new GridBagConstraints();
		gbc_lblCategoria.insets = new Insets(0, 0, 5, 5);
		gbc_lblCategoria.gridx = 3;
		gbc_lblCategoria.gridy = 8;
		frame.getContentPane().add(lblCategoria, gbc_lblCategoria);
		
		
		
		JLabel lblSubcategoria = new JLabel("Subcategoria");
		GridBagConstraints gbc_lblSubcategoria = new GridBagConstraints();
		gbc_lblSubcategoria.insets = new Insets(0, 0, 5, 5);
		gbc_lblSubcategoria.gridx = 3;
		gbc_lblSubcategoria.gridy = 9;
		frame.getContentPane().add(lblSubcategoria, gbc_lblSubcategoria);
		
		
		
		
		
		JLabel lblDescripcion = new JLabel("Descripcion");
		GridBagConstraints gbc_lblDescripcion = new GridBagConstraints();
		gbc_lblDescripcion.anchor = GridBagConstraints.EAST;
		gbc_lblDescripcion.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescripcion.gridx = 3;
		gbc_lblDescripcion.gridy = 10;
		frame.getContentPane().add(lblDescripcion, gbc_lblDescripcion);
		
		JTextPane _descripcion = new JTextPane();
		_descripcion.setEditable(propietario);
		_descripcion.setText(this.descripcion);
		GridBagConstraints gbc_descripcion = new GridBagConstraints();
		gbc_descripcion.insets = new Insets(0, 0, 5, 0);
		gbc_descripcion.gridheight = 2;
		gbc_descripcion.fill = GridBagConstraints.BOTH;
		gbc_descripcion.gridx = 4;
		gbc_descripcion.gridy = 10;
		frame.getContentPane().add(_descripcion, gbc_descripcion);
		if(propietario&&!nuevo){
			JButton btnEliminar = new JButton("Eliminar");
			btnEliminar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					articulo.deleteArticulo(id);
					
					Grid.grid.updateList();
					frame.dispose();
					
				}
			});
			GridBagConstraints gbc_btnEliminar = new GridBagConstraints();
			gbc_btnEliminar.insets = new Insets(0, 0, 0, 5);
			gbc_btnEliminar.gridx = 2;
			gbc_btnEliminar.gridy = 11;
			frame.getContentPane().add(btnEliminar, gbc_btnEliminar);
		}
		
		if(propietario){
			JButton guardar = new JButton("Guardar");
			guardar.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					float fprecio=Float.parseFloat(textPrecio.getText().replace('�', ' '));
					if(nuevo){
						
						articulo.addArticulo(textNombre.getText(), _descripcion.getText(),fprecio, chckbxEnvio.isSelected(), chckbxIntercambio.isSelected(), chckbxNegociable.isSelected(), (String)boxProvincia.getSelectedItem(), (String)boxLocalidad.getSelectedItem(),((SubCatego)boxSubCatego.getSelectedItem()).id);
						
					}else{
						articulo.updateArticulo(id,textNombre.getText(), _descripcion.getText(),fprecio, chckbxEnvio.isSelected(), chckbxIntercambio.isSelected(), chckbxNegociable.isSelected(), (String)boxProvincia.getSelectedItem(), (String)boxLocalidad.getSelectedItem(),((SubCatego)boxSubCatego.getSelectedItem()).id);
					}
					Grid.grid.updateList();
					frame.dispose();
					
				}
			});
			GridBagConstraints gbc_guardar = new GridBagConstraints();
			gbc_guardar.insets = new Insets(0, 0, 0, 5);
			gbc_guardar.gridx = 3;
			gbc_guardar.gridy = 11;
			frame.getContentPane().add(guardar, gbc_guardar);
		}
		
		
		JButton cancelar = new JButton("Cancelar");
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		GridBagConstraints gbc_cancelar = new GridBagConstraints();
		gbc_cancelar.anchor = GridBagConstraints.WEST;
		gbc_cancelar.gridx = 4;
		gbc_cancelar.gridy = 12;
		frame.getContentPane().add(cancelar, gbc_cancelar);
	}

}
