package wala;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Categoria {
	private static Map<Integer,Catego> categorias;
	
	public Categoria(Conexion con){
		categorias=new TreeMap<Integer,Catego>();
		
		
		ResultSet rs=con.getResultSet("categoria");
		try {
			rs.beforeFirst();
			while(rs.next()){
				categorias.put(rs.getInt(1),new Catego(rs.getInt(1),rs.getString(2), new ArrayList<SubCatego>()));
				
				Map<String,String> where= new TreeMap<String,String>();
				where.put("id_categoria",""+rs.getInt(1));
				ResultSet rs2=con.getResultSet("subcategoria", where);
				rs2.beforeFirst();
				while(rs2.next()){
					System.out.println(1);
					categorias.get(rs.getInt(1)).subcategorias.add(new SubCatego(rs2.getInt(1),rs2.getString(2), rs.getInt(1)));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Catego[] getCategorias(){
		
		return categorias.values().toArray(new Catego[0]);
		
	}
	public Catego getCatego(int id){
		return categorias.get(id);
	}
	
	public SubCatego[] getSubCategorias(int id_catego){
		return (SubCatego[]) categorias.get(id_catego).subcategorias.toArray(new SubCatego[0]);
	}
	public SubCatego getSubcatego(int id_subcatego){
		for(Map.Entry<Integer, Catego>map : categorias.entrySet()){
			SubCatego[] subca=getSubCategorias(map.getKey());
			for(int i=0;i<subca.length;i++){
				if(subca[i].id==id_subcatego){
					return subca[i];
				}
			}
			
		}
		return null;
	}
	public Catego findCatego(int id_subcatego){
		for(Map.Entry<Integer, Catego>map : categorias.entrySet()){
			SubCatego[] subca=getSubCategorias(map.getKey());
			for(int i=0;i<subca.length;i++){
				if(subca[i].id==id_subcatego){
					return getCatego(subca[i].id_catego);
				}
			}
			
		}
		return null;
	}
	
	
 	public class Catego{
		public int id;
		public String nombre;
		private ArrayList subcategorias;
		Catego(int id,String nombre,ArrayList subcategorias){
			this.id=id;
			this.subcategorias=subcategorias;
			this.nombre=nombre;
		}
		public String toString() {
		    return nombre;
		}
	}
	public class SubCatego{
		public int id;
		public int id_catego;
		public String nombre;
		
		SubCatego(int id,String nombre,int id_catego) {
			this.id=id;
			this.nombre=nombre;
			this.id_catego=id_catego;
		}
		public String toString() {
		    return nombre;
		}
		
	}
}
