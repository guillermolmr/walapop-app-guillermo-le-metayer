package wala;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import com.mysql.jdbc.Statement;
/**
 * @author Guillermo
 * Clase Conexion con metodos genericos para conectar con la Base de Datos.
 * @version 1.0
 */
public class Conexion {
		private String user;
		private String host="localhost";
		private String bd="wala";
		private Connection conexion;
		
		public Conexion(){
			try {
				this.conexion = DriverManager.getConnection("jdbc:mysql://localhost/"+bd,"root", "");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		
		/**
		 * Hace un select con los datos requeridos.
		 * @param table El nombre de la tabla.
		 * @param where Los filtros.
		 * @param start La fila por la que empieza a contar
		 * @param limit El numero de filas.
		 * @return Un ResultSet con los datos de la query.
		 */
		public ResultSet getResultSet(String table,Map<String,String> where, int start, int limit){
			ResultSet rs=null;
			
			try {
				Statement st = (Statement) conexion.createStatement();
				
				
				
				String query="Select * from "+table+" where ";
				for(Map.Entry<String, String> en : where.entrySet()){
					query+=" "+en.getKey()+" = '"+en.getValue()+"' and";
				}
				query=query.substring(0,query.length()-4);
				query+=" limit "+start+","+limit;
				//System.out.println(query);
				rs= st.executeQuery(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return rs;
			
		}
		
		
		/**
		 * Hace un select que devuelve una tabla entera
		 * @param table El nombre de la tabla.
		 * @return Un ResultSet con los datos de la query.
		 */
		public ResultSet getResultSet(String table){
			ResultSet rs=null;
			
			try {
				Statement st = (Statement) conexion.createStatement();
				String query="Select * from "+table;
				//System.out.println(query);
				rs= st.executeQuery(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return rs;
			
		}
		
		/**
		 * Hace un select con los datos requeridos sin limit.
		 * @param table El nombre de la tabla.
		 * @param where Los filtros.
		 * @return Un ResultSet con los datos de la query.
		 */
		public ResultSet getResultSet(String table,Map<String,String> where){
			ResultSet rs=null;
			
			try {
				Statement st = (Statement) conexion.createStatement();
				
				
				
				String query="Select * from "+table+" where ";
				for(Map.Entry<String, String> en : where.entrySet()){
					query+=" "+en.getKey()+" = '"+en.getValue()+"' and";
				}
				query=query.substring(0,query.length()-4);
				
				//System.out.println(query);
				rs= st.executeQuery(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return rs;
			
		}
		
		
		
		
		
		/**
		 * 
		 * @param table El nombre de la tabla
		 * @param data Los parametros a introducir.
		 * @return un int, si es 0 es que la query ha fallado, si es -1 es que ha habido un error de conexion y si es 1 es que el campo se ha introducido correctamente.
		 */
		public int insert(String table,Map<String,String>data ){
			int registros=0;
			data.put("venta", "0000-00-00 00:00:00");
			String query="INSERT INTO "+table+" (";
			String q=") values (";
			
			for(Map.Entry<String,String> map : data.entrySet()){
				query+=map.getKey()+",";
				if(map.getValue().equalsIgnoreCase("null")){
					q+=map.getValue()+",";
				}else{
					q+="'"+map.getValue()+"',";
				}
				
			}
			query=query.substring(0, query.length()-1);
			q=q.substring(0, q.length()-1);
			query+=q+")";
			try {
				Statement st = (Statement) conexion.createStatement();
				System.out.println(query);
				registros=st.executeUpdate(query);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				registros=-1;
				System.out.println("error!!");
			}
			return registros;
		}
		
		public int update(String table,Map<String,String>data,Map<String,String>filters){
			int registros=0;
			String query="UPDATE "+table+" SET ";
			String q=" WHERE ";
			
			for(Map.Entry<String,String> map : data.entrySet()){
				query+=map.getKey()+" = '"+map.getValue()+"',";
			}
			query=query.substring(0, query.length()-1);
			
			for(Map.Entry<String,String> map : filters.entrySet()){
				q+=" "+map.getKey()+" = '"+map.getValue()+"' and";
			}

			q=q.substring(0, q.length()-4);
			query+=q;
			try {
				Statement st = (Statement) conexion.createStatement();
				System.out.println(query);
				registros=st.executeUpdate(query);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return registros;
		}
		public int delete(String table,Map<String,String>data){
			int registros=0;
			
			String query="DELETE FROM "+table+" WHERE ";
			
			
			for(Map.Entry<String,String> map : data.entrySet()){
				query+=" "+ map.getKey()+" = "+map.getValue()+" and";
			}
			query=query.substring(0, query.length()-4);
			
			
		
			try {
				Statement st = (Statement) conexion.createStatement();
				registros=st.executeUpdate(query);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return registros;
		}
		public int count(String table,Map<String,String> where){
			ResultSet rs=null;
			int cantidad=0;
			try {
				Statement st = (Statement) conexion.createStatement();
				
				String query="Select count(*) from "+table+" where ";
				for(Map.Entry<String, String> en : where.entrySet()){
					query+=" "+en.getKey()+" = '"+en.getValue()+"' and";
				}
				query=query.substring(0,query.length()-4);
				
				System.out.println(query);
				rs= st.executeQuery(query);
				rs.beforeFirst();
				rs.next();
				cantidad=rs.getInt(1);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return cantidad;
		}
		
		
		
}
