package wala;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import UI.Grid;
import UI.VistaArticulo;

public class Articulos {
	private Conexion conexion;
	private Categoria catego;
	
	
	public Articulos(Conexion c){
		conexion=c;
	}
	public void addArticulo(String nombre,String descripcion,float precio,boolean envio,boolean intercambio,boolean negociable, String provincia,String localidad,int subcategoria){
		Map<String,String> data=new TreeMap<String,String>();
		data.put("id", "null");
		data.put("nombre", nombre);
		data.put("descripcion", descripcion);
		data.put("precio",precio+"");
		data.put("envio", ""+((envio)?1:0));
		data.put("intercambio", ""+((intercambio)?1:0));
		data.put("negociable", ""+((negociable)?1:0));
		Localidad.addLocalidad(conexion, provincia, localidad);
		data.put("id_localidad", (localidad));
		data.put("id_subcategoria", ""+(subcategoria));
		data.put("id_usuario", (User.getId()));
		conexion.insert("articulo", data);
	}
	
	public void updateArticulo(int id,String nombre,String descripcion,float precio,boolean envio,boolean intercambio,boolean negociable, String provincia,String localidad,int subcategoria){
		Map<String,String> data=new TreeMap<String,String>();
		
		data.put("nombre", nombre);
		data.put("descripcion", descripcion);
		data.put("precio",precio+"");
		data.put("envio", ""+((envio)?1:0));
		data.put("intercambio", ""+((intercambio)?1:0));
		data.put("negociable", ""+((negociable)?1:0));
		Localidad.addLocalidad(conexion, provincia, localidad);
		data.put("id_localidad", (localidad));
		data.put("id_subcategoria", ""+(subcategoria));
		data.put("id_usuario", (User.getId()));
		
		Map<String,String> filters=new TreeMap<String,String>();
		filters.put("id", ""+id);
		conexion.update("articulo", data, filters);
	}
	public void deleteArticulo(int id){
		Map<String,String> data=new TreeMap<String,String>();
		data.put("id", ""+id);
		conexion.delete("articulo", data);
		
	}
	
	public void getMisArticulos(JPanel panel,int pagina){
		
		Map<String,String> where= new TreeMap<String, String>();
		where.put("id_usuario", User.getId());
		ResultSet rs=conexion.getResultSet("articulo",where,(pagina-1)*5,5);
		Grid.num_paginas=conexion.count("articulo", where)/5+1;
		try {
			rs.beforeFirst();
			int i=0;
			while(rs.next()){
				i+=2;
				Listar(panel,i,rs.getInt(1),"",rs.getString(2),rs.getString(3),rs.getFloat(4),rs.getBoolean(5),rs.getBoolean(6),rs.getBoolean(7),rs.getString(8),rs.getString(9),rs.getInt(10));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public void buscarArticulos(JPanel panel,int pagina,String nombre,int precio, boolean envio,boolean intercambiable, boolean negociable,String localidad ){
	
		Map<String,String> map=new TreeMap<String,String>();
	
		if(nombre.length()>0)
			map.put("nombre", nombre);
		if(precio>0)
			map.put("precio <", ""+precio);
		if(envio)
			map.put("envio", ""+1);
		if(intercambiable)
			map.put("intercambio", ""+1);
		if(negociable)
			map.put("negociable", ""+1);
		System.out.println("l:"+localidad);
		if(localidad.length()>0)
			map.put("localidad", localidad);
		ResultSet rs=conexion.getResultSet("articulo",map,(pagina-1)*5,5);
		Grid.num_paginas=conexion.count("articulo", map)/5+1;
		try {
			rs.beforeFirst();
			int i=0;
			while(rs.next()){
				i+=2;
				Listar(panel,i,rs.getInt(1),"",rs.getString(2),rs.getString(3),rs.getFloat(4),rs.getBoolean(5),rs.getBoolean(6),rs.getBoolean(7),rs.getString(8),rs.getString(9),rs.getInt(10));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void buscarSubCategoria(JPanel panel,int subcat,int pagina){
		Map<String,String> map=new TreeMap<String,String>();
		map.put("id_subcategoria", ""+subcat);
		ResultSet rs=conexion.getResultSet("articulo",map,(pagina-1)*5,5);
		Grid.num_paginas=conexion.count("articulo", map)/5+1;
		try {
			rs.beforeFirst();
			int i=0;
			while(rs.next()){
				i+=2;
				Listar(panel,i,rs.getInt(1),"",rs.getString(2),rs.getString(3),rs.getFloat(4),rs.getBoolean(5),rs.getBoolean(6),rs.getBoolean(7),rs.getString(8),rs.getString(9),rs.getInt(10));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void Listar(JPanel panel,int _row,int id,String url,String nombre,String descripcion, float precio,boolean envio,boolean intercambiable, boolean negociable, String localidad,String vendedor,int subcategoria){
		final int final_id=id,final_subcategoria=subcategoria;
		final String final_nombre=nombre,final_descripcion=descripcion,final_localidad=localidad,final_vendedor=vendedor;
		final float final_precio=precio;
		final boolean final_envio=envio,final_intercambiable=intercambiable,final_negociable=negociable;
		
		int row=_row*2;
		JLabel lblFoto = new JLabel();
		lblFoto.setIcon(new ImageIcon(url));
		GridBagConstraints gbc_lblFoto = new GridBagConstraints();
		gbc_lblFoto.insets = new Insets(0, 0, 0, 5);
		gbc_lblFoto.gridx = 0;
		gbc_lblFoto.gridy = row;
		panel.add(lblFoto, gbc_lblFoto);
		
		JLabel lblNombre = new JLabel(nombre);
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.insets = new Insets(0, 0, 0, 5);
		gbc_lblNombre.gridx = 1;
		gbc_lblNombre.gridy = row;
		panel.add(lblNombre, gbc_lblNombre);
		
		JLabel lblPrecio = new JLabel(precio+"�");
		GridBagConstraints gbc_lblPrecio = new GridBagConstraints();
		gbc_lblPrecio.insets = new Insets(0, 0, 0, 5);
		gbc_lblPrecio.gridx = 2;
		gbc_lblPrecio.gridy = row;
		panel.add(lblPrecio, gbc_lblPrecio);
		
		JCheckBox CheckBoxEnvio = new JCheckBox();
		
		CheckBoxEnvio.setEnabled(false);
		CheckBoxEnvio.setSelected(envio);
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxNewCheckBox.gridx = 3;
		gbc_chckbxNewCheckBox.gridy = row;
		panel.add(CheckBoxEnvio, gbc_chckbxNewCheckBox);
		
		JCheckBox chckbxIntercambio = new JCheckBox("");
		chckbxIntercambio.setEnabled(false);
		chckbxIntercambio.setSelected(intercambiable);
		GridBagConstraints gbc_chckbxIntercambio = new GridBagConstraints();
		gbc_chckbxIntercambio.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxIntercambio.gridx = 4;
		gbc_chckbxIntercambio.gridy = row;
		panel.add(chckbxIntercambio, gbc_chckbxIntercambio);
		
		JCheckBox chckbxNegociable = new JCheckBox("");
		chckbxNegociable.setEnabled(false);
		chckbxNegociable.setSelected(negociable);
		GridBagConstraints gbc_chckbxNegociable = new GridBagConstraints();
		gbc_chckbxNegociable.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxNegociable.gridx = 5;
		gbc_chckbxNegociable.gridy = row;
		panel.add(chckbxNegociable, gbc_chckbxNegociable);
		
		JLabel lblLocalidad = new JLabel(localidad);
		GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
		gbc_lblLocalidad.insets = new Insets(0, 0, 0, 5);
		gbc_lblLocalidad.gridx = 6;
		gbc_lblLocalidad.gridy = row;
		panel.add(lblLocalidad, gbc_lblLocalidad);
		
		JLabel lblUsuario = new JLabel(vendedor);
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.insets = new Insets(0, 0, 0, 5);
		gbc_lblUsuario.gridx = 7;
		gbc_lblUsuario.gridy = row;
		panel.add(lblUsuario, gbc_lblUsuario);
		
		
		
		
		JButton jbutton= new JButton((User.getId().equals(vendedor))?"Editar":"Ver");
		jbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				VistaArticulo vista= new VistaArticulo(Grid.articulos, User.getId().equals(final_vendedor), false, final_id, final_nombre, final_descripcion, final_precio,final_envio,final_intercambiable,final_negociable, final_vendedor, final_localidad,final_subcategoria);
				vista.frame.setVisible(true);
			}
		});
		GridBagConstraints gbc_jbutton = new GridBagConstraints();
		gbc_jbutton.weighty = 0.5;
		gbc_jbutton.anchor = GridBagConstraints.NORTH;
		gbc_jbutton.insets = new Insets(0, 0, 0, 5);
		gbc_jbutton.gridx = 8;
		gbc_jbutton.gridy = row+1;
		panel.add(jbutton, gbc_jbutton);
		
		
		Component rigidArea = Box.createRigidArea(new Dimension(20, 20));
		GridBagConstraints gbc_rigidArea = new GridBagConstraints();
		gbc_rigidArea.weighty = 0.5;
		gbc_rigidArea.anchor = GridBagConstraints.NORTH;
		gbc_rigidArea.insets = new Insets(0, 0, 0, 5);
		gbc_rigidArea.gridx = 9;
		gbc_rigidArea.gridy = row+1;
		panel.add(rigidArea, gbc_rigidArea);
		
		
	}
	public int contarMisArticulos(){
		Map<String,String> map=new TreeMap<String,String>();
		map.put("id_usuario",User.getId());
		return conexion.count("articulo", map);
		
	}
	
	
	
	
}
