package wala;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

public class User {
	private static String id;
	private static String nick;
	private static Conexion con;
	public static boolean logUsu(Conexion conexion, String mail, String pass){
		Map<String,String> where =new TreeMap<String,String>();
		where.put("mail", mail);
		where.put("pass",md5(pass));
		
		ResultSet rs=conexion.getResultSet("usuarios", where, 0, 1);
		
		try {
			rs.beforeFirst();
			if(rs.next()){
				id=rs.getString(1);
				nick=rs.getString(3);
				con=conexion;
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}
	public static String getId(){
		return id;
	}
	public static String getNick(){
		return nick;
	}
	public static Conexion getConexion(){
		return con;
	}
	public static String md5(String input) {
		
		String md5 = null;
		
		if(null == input) return null;
		
		try {
			
		//Create MessageDigest object for MD5
		MessageDigest digest = MessageDigest.getInstance("MD5");
		
		//Update input string in message digest
		digest.update(input.getBytes(), 0, input.length());

		//Converts message digest value in base 16 (hex) 
		md5 = new BigInteger(1, digest.digest()).toString(16);

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return md5;
	}
}
